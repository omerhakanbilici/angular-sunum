(function (module){
  module.factory("apiSubreddit", apiSubreddit);

  apiSubreddit.$inject = ["$http"];
  function apiSubreddit($http) {
    return {
      getLinks(subreddit, type){
        var url = "https://www.reddit.com/r/" + subreddit + "/" + type + ".json";

        return $http.get(url);
      }
    }
  }

})(angular.module("app.api"));
